#include "stdAfx.h"
#include "Assignment4.h"

// Assignment4 Source File 

////////////////////////////////////////////////////////////////////////////////
// A brief description of C2DPoint and C3DPoint
//
// class C2DPoint
// {
// public:
//		double x; // stores the x coordinate
//		double y; // stores the y coordinate
// };
//
// class C3DPoint
// {
// public:
//		double x; // stores the x coordinate
//		double y; // stores the y coordinate
//		double z; // stores the y coordinate
// };
//
// Note:
// Although C2DPoint and C3DPoint do contain other data members,
// don't bother with them
//
//void initialize1(CMatrix<double>& P, vector<C3DPoint*>& pt3D, vector<C2DPoint*>& pt2D, CMatrix<double>& P2);
void initialize1(CMatrix<double>& P,const vector<C3DPoint*>& pt3D,const vector<C2DPoint*>& pt2D);
//void initialize2(CMatrix<double>& P, vector<C3DPoint*>& pt3D, vector<C2DPoint*>& pt2D, CMatrix<double>& P2);
void initialize2(CMatrix<double>& P,const vector<C3DPoint*>& pt3D,const vector<C2DPoint*>& pt2D);
void initialize(CMatrix<double>& P,vector<C3DPoint*> pt3D,vector<C2DPoint*> pt2D);

BOOL CCamera::Calibrate(const vector<C2DPoint*>& src2D, const vector<C3DPoint*>& src3D, 
						const vector<C2DPoint*>& corners, CMatrix<double>& matPrj)
{

	matPrj.SetSize(3,4,0);

	// Step 1: Classify the input 3D points into points on the x-z planes, points on
	//         the y-z plane, and points not on any calibration plane

	vector<C3DPoint*> xz3D, yz3D, no3D;
	vector<C2DPoint*> xz2D, yz2D, no2D;

	for ( int i = 0 ; i < src3D.size() ; i++){
		if ((src3D[i])->y == 0)
		{
			xz3D.push_back((src3D[i]));
			xz2D.push_back((src2D[i]));
		}
		else if ((src3D[i])->x == 0)
		{
			yz3D.push_back((src3D[i]));
			yz2D.push_back((src2D[i]));
		}else{
			no3D.push_back((src3D[i]));
			no2D.push_back((src2D[i]));
		}
	}

	// Step 2: Estimate a plane-to-plane projectivity for each of the calibration planes
	//         using the input 2D/3D point pairs
	
	CMatrix<double> A(2*xz3D.size(),9), B(2*yz3D.size(),9), U, D, V, P, Pxz, Pyz;
	initialize1(A,xz3D,xz2D);
	A.SVD2( U, D, V);
	//V.Transpose();
	P = V.GetCol(D.Rows()-1);
	Pxz.SetSize(3, 3, 0);


	for (int j = 0; j < 3 ; j++){
		for (int k = 0 ; k < 3 ; k++)
		{
			Pxz(j,k) = P(j*3 + k,0) / P(8,0) ;
		}
	}
		
	CMatrix<double> U2, D2, V2, P2;
	initialize2(B,yz3D,yz2D);
	B.SVD2( U2, D2, V2);
	//V.Transpose();
	P2 = V2.GetCol(D2.Rows()-1);
	Pyz.SetSize(3, 3, 0);

	for (int j = 0; j < 3 ; j++){
		for (int k = 0 ; k < 3 ; k++)
		{
			Pyz(j,k) = P2(j*3 + k,0) / P2(8,0) ;
		}
	}


	// Step 3: Using the estimated plane-to-plane projectivities, assign 3D coordinates
	//         to all the detected corners on the calibration pattern

		vector<C3DPoint*> corners3D;
		vector<C2DPoint*> corners2D;

		C3DPoint* Cxz3D;
		C3DPoint* Cyz3D;
		C2DPoint* Cxz2D;
		C2DPoint* Cyz2D;

		int k = 0 ;
		double range = 0.05;
		bool again = false;

		CMatrix<double> M(3,1,1);
		for (int i = 0 ; i < 10 ; i++){
			for (int j = 0 ; j < 8 ; j++)
			{
				k = 0 ;
				Cxz3D = new C3DPoint(0.5 + i, 0, 0.5 + j);
				Cxz2D = new C2DPoint(0,0);
				M(0,0) = Cxz3D->x ;
				M(1,0) = Cxz3D->z ;
				Cxz2D->x = (Pxz * M)(0,0) / (Pxz * M)(2,0);
				Cxz2D->y = (Pxz * M)(1,0) / (Pxz * M)(2,0);

				for ( int k = 0 ; k < corners.size() ; k++)
					if (abs(corners[k]->x - Cxz2D->x) < range && abs(corners[k]->y - Cxz2D->y) < range)
					{
						if (again == false){
							Cxz2D->x = corners[k]->x;
							Cxz2D->y = corners[k]->y;

							corners2D.push_back(Cxz2D);
							corners3D.push_back(Cxz3D);

							again = true;
						}
						else 
							;
					}


				again = false ;

				Cyz3D = new C3DPoint(0, 0.5 + i, 0.5 + j);
				Cyz2D = new C2DPoint(0,0);

				M(0,0) = Cyz3D->y ;
				M(1,0) = Cyz3D->z ;
				Cyz2D->x = (Pyz * M)(0,0) / (Pyz * M)(2,0);
				Cyz2D->y = (Pyz * M)(1,0) / (Pyz * M)(2,0);

				for ( int k = 0 ; k < corners.size() ; k++)
					if (abs(corners[k]->x - Cyz2D->x) < range && abs(corners[k]->y - Cyz2D->y) < range)
					{
						if (again == false){
							Cyz2D->x = corners[k]->x;
							Cyz2D->y = corners[k]->y;

							corners2D.push_back(Cyz2D);
							corners3D.push_back(Cyz3D);

							again = true;
						}
						else
							;
					}
			}
		}


	// Step 4: Estimate a 3x4 camera projection matrix from all the detected corners on
	//         the calibration pattern using linear least squares
		CMatrix<double> C(2*corners3D.size(),12), U3, D3, V3, P3; 
		initialize(C, corners3D, corners2D);
		C.SVD2( U3, D3, V3);
		//V1.Transpose();

		again = false;

		for (int a = D3.Rows() -1 ; a >= 0 ; a--)
		{
			//if (D1(a,0)  !=  0 )
		//	{
				if (again == false){
					P3 = V3.GetCol(D3.Rows() - 1);

					for (int b = 0; b < 3; b++)
						for (int c = 0 ; c < 4 ; c++)
							matPrj(b,c) = P3(b*4 + c,0) / P3(11,0) ;


					again = true;
				}
				else
					;
		}
		
	return TRUE;
}

void initialize1(CMatrix<double>& P,const vector<C3DPoint*>& pt3D,const vector<C2DPoint*>& pt2D){
	for (int i = 0 ; i < pt3D.size(); i ++)
	{
			P(i*2,0) = pt3D[i]->x;
			P(i*2,1) = pt3D[i]->z;
			P(i*2,2) = 1;
			P(i*2,3) = 0;
			P(i*2,4) = 0;
			P(i*2,5) = 0;
			P(i*2,6) = -pt2D[i]->x * pt3D[i]->x ;
			P(i*2,7) = -pt2D[i]->x * pt3D[i]->z ;
			P(i*2,8) = -pt2D[i]->x ; 

			P(i*2+1,0) = 0 ;
			P(i*2+1,1) = 0 ;
			P(i*2+1,2) = 0 ;
			P(i*2+1,3) = pt3D[i]->x ;
			P(i*2+1,4) = pt3D[i]->z ;
			P(i*2+1,5) = 1;
			P(i*2+1,6) = -pt2D[i]->y * pt3D[i]->x ;
			P(i*2+1,7) = -pt2D[i]->y * pt3D[i]->z ;
			P(i*2+1,8) = -pt2D[i]->y;
	}
}

void initialize2(CMatrix<double>& P,const vector<C3DPoint*>& pt3D,const vector<C2DPoint*>& pt2D){
	for (int i = 0 ; i < pt3D.size(); i ++)
	{
			P(i*2,0) = pt3D[i]->y;
			P(i*2,1) = pt3D[i]->z;
			P(i*2,2) = 1;
			P(i*2,3) = 0;
			P(i*2,4) = 0;
			P(i*2,5) = 0;
			P(i*2,6) = -pt2D[i]->x * pt3D[i]->y ;
			P(i*2,7) = -pt2D[i]->x * pt3D[i]->z ;
			P(i*2,8) = -pt2D[i]->x;

			P(i*2+1,0) = 0 ;
			P(i*2+1,1) = 0 ;
			P(i*2+1,2) = 0 ;
			P(i*2+1,3) = pt3D[i]->y ;
			P(i*2+1,4) = pt3D[i]->z ;
			P(i*2+1,5) = 1;
			P(i*2+1,6) = -pt2D[i]->y * pt3D[i]->y ;
			P(i*2+1,7) = -pt2D[i]->y * pt3D[i]->z ;
			P(i*2+1,8) = -pt2D[i]->y;
	}
}


void initialize(CMatrix<double>& P,vector<C3DPoint*> pt3D,vector<C2DPoint*> pt2D){
	for (int i = 0 ; i < pt3D.size(); i ++)
	{
			P(i*2,0) = pt3D[i]->x;
			P(i*2,1) = pt3D[i]->y;
			P(i*2,2) = pt3D[i]->z;
			P(i*2,3) = 1;
			P(i*2,4) = 0;
			P(i*2,5) = 0;
			P(i*2,6) = 0;
			P(i*2,7) = 0;
			P(i*2,8) = -pt2D[i]->x * pt3D[i]->x ;
			P(i*2,9) = -pt2D[i]->x * pt3D[i]->y ;
			P(i*2,10) = -pt2D[i]->x * pt3D[i]->z ;
			P(i*2,11) = -pt2D[i]->x ;

			P(i*2+1,0) = 0 ;
			P(i*2+1,1) = 0 ;
			P(i*2+1,2) = 0 ;
			P(i*2+1,3) = 0 ;
			P(i*2+1,4) = pt3D[i]->x ;
			P(i*2+1,5) = pt3D[i]->y ;
			P(i*2+1,6) = pt3D[i]->z ;
			P(i*2+1,7) = 1 ;
			P(i*2+1,8) = -pt2D[i]->y * pt3D[i]->x ;
			P(i*2+1,9) = -pt2D[i]->y * pt3D[i]->y ;
			P(i*2+1,10) = -pt2D[i]->y * pt3D[i]->z ;
			P(i*2+1,11) = -pt2D[i]->y ;
	}
}

void CCamera::Decompose(const CMatrix<double>& prjMatrix, CMatrix<double>& prjK, CMatrix<double>& prjRt)
{
	// INPUT:
	//     CMatrix<double>& prjMatrix    This is a 3x4 camera projection matrix to be decomposed.
	//
	// OUTPUT:
	//     CMatrix<double>& prjK         This is the 3x3 camera calibration matrix K.
	//
	//     CMatrix<double>& prjRt        This is the 3x4 matrix composed of the rigid body motion of the camera.
	//
	// Please refer to the tutorial for the usage of the related libraries.

	prjK.SetSize(3,3,0);
	prjRt.SetSize(3,4,0);

	//////////////////////////
	// Begin your code here
	
	// Step 1: Decompose the 3x3 sub-matrix composed of the first 3 columns of
	//         prjMatrix into the product of K and R using QR decomposition

	CMatrix<double> Temp(3,3,0), Q, R, Rotation, K;

	//Temp = KR;
	for (int i = 0 ; i < 3 ; i ++)
		for (int j = 0 ; j < 3 ; j ++)
			Temp(i,j) = prjMatrix(i,j);

	Temp.Inverse(Temp);

	Temp.QR( Q, R);

	Q.Transpose(Rotation);

	R.Inverse(K);

	double a = K(2,2);

	prjK = K / K(2,2);

	// Step 2: Compute the translation vector T from the last column of prjMatrix

	CMatrix<double> p3 = prjMatrix.GetCol(3) ;
	CMatrix<double> T = (1/a) * prjK.Inverse() * p3;

	prjRt.SetCol(0,Rotation.GetCol(0));
	prjRt.SetCol(1,Rotation.GetCol(1));
	prjRt.SetCol(2,Rotation.GetCol(2));
	prjRt.SetCol(3,T);

	// Step 3: Normalize the 3x3 camera calibration matrix K

	for ( int i = 0 ; i < 3 ; i ++)
		for (int j = 0 ; j < 3 ; j++){
			K(i,j) /= K(2,2);
			prjK(i,j) = K(i,j);
		}

	return;
}

void CCamera::Triangulate(const vector<CMatrix<double>*>& prjMats, const vector<vector<C2DPoint*>*>& src2Ds,
							vector<C3DPoint*>& res3D)
{
	// INPUT:
	//     vector<CMatrix<double>*> prjMats 	A list of projection matrices
	//
	//     vector<vector<C2DPoint*>*> src2Ds	A list of image point lists, each image point list is in 1-to-1
	//                                          correspondence with the projection matrix having the same index in prjMats.
	//
	// OUTPUT:
	//     vector<C3DPoint*> res3D				A list of 3D coordinates for the triangulated points.
	//
	// Note:
	//    - src2Ds can be considered as a 2D array with each 'column' containing the image positions 
	//      for the same 3D point in different images. If any of the image does not contain the image for a particular
	//      point, the corresponding element in src2Ds will be a Null vector. For example, if there are two images, 
	//      and we know 8 pairs of corresponding points, then
	//      
	//			prjMats.size() = 2
	//			src2Ds.size() = 2
	//			src2Ds[k]->size() = 8           // k >= 0 and k < no. of images - 1
	//    
	//    - If for any reason the 3D coordinates corresponding to a 'column' in src2Ds cannot be computed,
	//      please push in a NULL as a place holder. That is, you have to make sure that each point in res3D
	//      must be in 1-to-1 correspondence with a column in src2Ds, i.e., 
	//      
	//			src2Ds[k]->size() == src3D.size()	// k >= 0 and k < no. of images - 1
	//
	// Please refer to the tutorial for the usage of related libraries.

	//////////////////////////
	// Begin your code here


	CMatrix<double> A (2*src2Ds.size(),4,0), U, D, V, P;
	bool go;
	double x, y, z;

	for (int i = 0 ; i < src2Ds[0]->size() ; i ++){

		go = true;

		for (int j = 0 ; j < prjMats.size(); j++)
		{
			if (go == true){
				if (src2Ds[j]->at(i) != NULL){
					z = prjMats[j]->GetCol(0)(2,0);
					x = (src2Ds[j]->at(i))->x ;
					y = prjMats[j]->GetCol(1)(0,0);
					A(j*2,0) = prjMats[j]->GetCol(0)(0,0) - ((src2Ds[j]->at(i))->x * prjMats[j]->GetCol(0)(2,0)) ;
					A(j*2,1) = prjMats[j]->GetCol(1)(0,0) - ((src2Ds[j]->at(i))->x * prjMats[j]->GetCol(1)(2,0)) ;
					A(j*2,2) = prjMats[j]->GetCol(2)(0,0) - ((src2Ds[j]->at(i))->x * prjMats[j]->GetCol(2)(2,0)) ;
					A(j*2,3) = prjMats[j]->GetCol(3)(0,0) - ((src2Ds[j]->at(i))->x * prjMats[j]->GetCol(3)(2,0)) ;

					A(j*2+1,0) = prjMats[j]->GetCol(0)(1,0) - ((src2Ds[j]->at(i))->y * prjMats[j]->GetCol(0)(2,0)) ;
					A(j*2+1,1) = prjMats[j]->GetCol(1)(1,0) - ((src2Ds[j]->at(i))->y * prjMats[j]->GetCol(1)(2,0)) ;
					A(j*2+1,2) = prjMats[j]->GetCol(2)(1,0) - ((src2Ds[j]->at(i))->y * prjMats[j]->GetCol(2)(2,0)) ;
					A(j*2+1,3) = prjMats[j]->GetCol(3)(1,0) - ((src2Ds[j]->at(i))->y * prjMats[j]->GetCol(3)(2,0)) ;
				}
				else
					go = false;
			}
		}

		if (go == true){
			A.SVD2( U, D, V);
			double x = D(1,0);
			double y = D(2,0);
			double z = D(3,0);
			x = D.Rows() - 1;
			P = V.GetCol(x);
			x = P(0,0);
			y = P(1,0);
			z = P(2,0);
			x = P(3,0);
			//P = P / P(3,0);
			C3DPoint* pPnt = new C3DPoint(P(0,0)/P(3,0),P(1,0)/P(3,0),P(2,0)/P(3,0));
			//if (pPnt->x > 0 && pPnt->y > 0 && pPnt->z > 0 )
				res3D.push_back(pPnt);
		}
		else
		{
			res3D.push_back(NULL);
		}
	}


	return;
}
